#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	//
	const int n = 5;
	//

	int r[n][n]{};

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			r[i][j] = i + j;
			cout << r[i][j] << " ";
		}
		cout << endl;
	}

	int sum = 0;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			sum = sum + r[i][j];
		}
	}

	cout << buf.tm_mday << "% of the sum is " << (sum / 100) * buf.tm_mday;
}